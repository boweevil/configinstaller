#!/bin/bash
set -euo pipefail
trap exit_trap EXIT
trap err_trap ERR
showed_traceback=f


function log() {
	local message="$1"
	local type="$2"
	local timestamp=$(date '+%Y-%m-%d %H:%M:%S')
	local color
	local endcolor="\033[0m"

	case "$type" in
		"info")
			color="\033[38;5;79m" ;;
		"success")
			color="\033[1;32m" ;;
		"warn")
			color="\033[1;33m" ;;
		"error")
			color="\033[1;31m" ;;
		*)
			color="\033[1;34m" ;;
	esac

	echo -e "${color}${timestamp} - ${message}${endcolor}"
}


function except() {
	local exit_status message
	if is_integer "$1"; then
		exit_status="${1:-1}"
		message="${2:-}"
	else
		exit_status=1
		message="${1:-}"
	fi

	trap - EXIT

	traceback 1

	if [[ -n "${message}" ]]; then
		log "${message}: Exiting with status: ${exit_status}" "error"
		# printf '%s: Exiting with status: %s\n' "${message}" "${exit_status}"
		exit "${exit_status}"
	fi

	log "Exiting with status: ${exit_status}" "error"
	# printf 'Exiting with exit_status: %s\n' "${exit_status}"
	exit "${exit_status}"
}

function exit_trap() {
	local ec
	ec="$?"
	if [[ $ec != 0 && "${showed_traceback}" != t ]]; then
		traceback 1
	fi
}

function err_trap() {
	local ec cmd
	ec="$?"
	cmd="${BASH_COMMAND:-unknown}"
	traceback 1
	showed_traceback=t
	echo "The command ${cmd} exited with exit code ${ec}." 1>&2
}

function traceback() {
	# Hide the traceback() call.
	local -i start=$(( ${1:-0} + 1 ))
	local -i end=${#BASH_SOURCE[@]}
	local -i i=0
	local -i j=0

	traceback_output=()
	echo "Traceback:" 1>&2
	for ((i=start; i < end; i++)); do
		j=$(( i - 1 ))
		local function file line
		function="${FUNCNAME[$i]}"
		file="$( realpath "${BASH_SOURCE[$i]}" )"
		line="${BASH_LINENO[$j]}"
		traceback_output+=("${function}() in ${file}:${line}")
	done
	printf '    %s\n' "${traceback_output[@]}" | tac
}


function is_integer() {
	if [[ $1 =~ ^[0-9]+$ ]] ; then
		return 0
	fi

	return 1
}
