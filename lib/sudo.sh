#!/usr/bin/env bash


[[ $(whoami) != 'root' ]] \
	&& sudo='sudo' \
	|| sudo=''

export sudo
