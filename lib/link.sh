#!/usr/bin/env bash

function linux_stat() {
	local target="${1:-}"
	stat -c %F "${target}"
}


function unix_stat() {
	local target="${1:-}"
	stat -f "%HT" "${target}"
}


function identify_target() {
	local target="${1:-}"
	case $(uname -s) in
		'Linux')
			linux_stat "${target}" | tr '[:upper:]' '[:lower:]'
			return 0
			;;
		'Darwin'|'FreeBSD')
			unix_stat "${target}" | tr '[:upper:]' '[:lower:]'
			return 0
			;;
	esac
	except 1 "Failed to identify ${target}."
}


function handle_existing() {
	local target="${1:-}"
	local source="${2:-}"
	filetype="$(identify_target "${target}")"
	case "${filetype}" in
		'symbolic link')
			if [[ "$(realpath "${target}")" == "${source}" ]]; then
				log "Target link, '${target}', is already installed." "warn"
				return 1  # It's installed so leave 'handled' set to false so the link isn't attempated
			fi
			mv "${target}" "${target}-$(date +%Y%m%d)"
			return 0
			;;
		'regular file'|'directory')
			mv "${target}" "${target}-$(date +%Y%m%d)"
			return 0
			;;
		*)
			except 1 "Failed to handle existing target '${target}'"
			;;
	esac
}


function create_parent_directory() {
	local target="${1:-}"
	local target_dir="$( dirname ${target} )"
	log "Creating ${target_dir}..." "info"
	mkdir -vp "${target_dir}"
}


function install_link() {
	source="${1:-}"
	target="${2:-}"

	handled=false
	create_parent_directory "${target}"
	if [[ -e "${target}" ]] || [[ -L "${target}" ]]; then
		handle_existing "${target}" "${source}" \
			&& handled=true
	else
		handled=true
	fi

	if [[ "${handled}" == true ]]; then
		log "Installing ${source} to ${target}." "info"
		ln -s "${source}" "${target}" \
			&& log "Installed ${source} to ${target}." "success"
	fi

	return 0
}
