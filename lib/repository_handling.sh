#!/usr/bin/env bash


function convert_git_ssh_to_http() {
	local repo=${1:-}
	echo "${repo}" \
		| sed -e 's#:#/#' \
			-e 's#^.*@#https://#'
}


function try_clone() {
	local repo=${1:-}
	local target_directory=${2:-}

	log "Attempting ssh URL." "info"
	git clone "${repo}" "${target_directory}" && return 0
	log "Switching to https URL." "info"
	git clone "$(convert_git_ssh_to_http "${repo}")" "${target_directory}" && return 0
	except 1 "Failed to clone ${repo}."
}


function clone_repo() {
	local repo=${1:-}
	local target_directory=${2:-}

	if [[ ! -d "${target_directory}" ]]; then
		try_clone "${repo}" "${target_directory}"
		log "'${repo}' installed to '${target_directory}'." "info"
		return 0
	fi

	target_dir_id="$(git -C "${target_directory}" remote get-url --all origin 2>/dev/null)"
	if [[ "${target_dir_id}" != "${repo}" ]] && [[ "${target_dir_id}" != "$(convert_git_ssh_to_http "${repo}")" ]]; then
		backup_directory="${target_directory}-$(date +%Y%m%d)"
		mv "${target_directory}" "${backup_directory}"
		log "Existing directory '${target_directory}' backed up to '${backup_directory}'." "info"
		try_clone "${repo}" "${target_directory}"
		log "'${repo}' installed to '${target_directory}'." "info"
		return 0
	fi

	log "'${repo}' is already installed to '${target_directory}'." "info"
	return 0
}
