#!/usr/bin/env bash

function get_os_data() {
    if [[ ! -f /etc/os-release ]]; then
        return 1
    fi
    source /etc/os-release
    printf '%s' $ID
    return 0
}


function get_lsb_release() {
    if ! command -v lsb_release 1>/dev/null; then
        return 1
    fi
    printf '%s' $(lsb_release --id --short)
    return 0
}


function detect_distribution() {
    distro=$(get_lsb_release) \
        || distro=$(get_os_data)
    if [[ -n ${distro} ]]; then
        printf '%s' ${distro}
        return 0
    fi
    echo "Failed to detect distribution."
    return 1
}


function get_package_manager() {
	case ${1} in
		'debian'|'ubuntu')
			package_manager='debian'
			;;
		'fedora'|'redhat')
			package_manager='redhat'
			;;
		'arch')
			package_manager='arch'
			;;
		*)
			except 1 "Unable to find package manager."
			;;
	esac
	printf '%s' ${package_manager}
	return 0
}


function inspect_distro() {
	distro=$1
	package_manager=$(get_package_manager ${distro})
}
