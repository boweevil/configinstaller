#!/usr/bin/env bash

function install_missing_packages() {
	if [[ -z "$*" ]]; then
		except 1 "Provide a list of packages to be installed."
	fi

	input_packages=($@)
	declare -a output_packages

	for package in "${!input_packages[@]}" ; do
		if ! dpkg -s "${input_packages[package]}" &>/dev/null; then
			output_packages+=( "${input_packages[$package]}" )
		fi
	done

	if [[ -n "${output_packages[*]}" ]]; then
		echo "Installing missing packages ${output_packages[*]}"
		$sudo apt update \
			&& $sudo apt install -y "${output_packages[@]}"
	fi
}
