NAME ?= dotdeploy
CONTAINER_TOOL ?= podman
CONTAINER_CMD ?= $(CONTAINER_TOOL) run \
		--rm \
		--volume $(PWD):/app \
		--workdir=/app \
		--interactive \
		--tty \
		--entrypoint=/bin/bash

.PHONY: setup
setup:
	@bash setup.sh

.PHONY: test-ubuntu
test-ubuntu:
	@$(CONTAINER_CMD) \
		--name=$(NAME)-ubuntu \
		ubuntu:latest

.PHONY: test-fedora
test-fedora:
	@$(CONTAINER_CMD) \
		--name=$(NAME)-fedora \
		fedora:latest
