#!/usr/bin/env bash
# Setup dotfiles on local system

set -euo pipefail
IFS=$'\n\t'

SCRIPT_NAME="$( basename "$0" )"; export SCRIPT_NAME
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd ); export SCRIPT_DIR
SCRIPT_PATH="$SCRIPT_DIR/$SCRIPT_NAME"; export SCRIPT_PATH

cd "$SCRIPT_DIR";

source "$SCRIPT_DIR/lib/error_handling.sh";
source "$SCRIPT_DIR/lib/distro_detect.sh";
source "$SCRIPT_DIR/lib/sudo.sh";
source "$SCRIPT_DIR/lib/repository_handling.sh";
source "$SCRIPT_DIR/lib/link.sh";

MODULES_FILE="modules.json"


function main() {
	case $(uname -s) in
		'Linux')
			distro=$(detect_distribution)
			log "Linux distribution is ${distro}." "info"
			log "Running Linux setup scripts..." "info"
			package_manager_type=$(get_package_manager "${distro}")
			;;
		'Darwin')
			distro=macos
			package_manager_type='brew'
			log "Running MacOS setup scripts..." "info"
			;;
		*)
			except 1 "Unsupported operating system!"
			;;
	esac
	source "$SCRIPT_DIR/lib/pkgmgr_${package_manager_type}.sh"
	echo
	install_missing_packages \
		curl \
		git \
		jq \
	|| except 1 "Failed to install required packages."

	log "Installing module packages..." "info"

	install_missing_packages "$(jq .[] "$MODULES_FILE" | jq -r .packages[] | sort | uniq)" \
		|| except 1 "Failed to install module-required packages."

	log "Installing repositories..." "info"
	jq -r 'keys[]' "$MODULES_FILE" | while read -r module; do
		repo=$(jq -r ".$module.repo" "$MODULES_FILE")
		target_directory=$(jq -r ".$module.target_directory" "$MODULES_FILE")
		target_directory="$(eval echo ${target_directory})"  # This is done to expand env vars like $HOME
		log "Installing ${module} from ${repo} to ${target_directory}..." "info"
		clone_repo "${repo}" "${target_directory}" \
			|| except 1 "Failed to clone ${repo}"
		echo
	done

	log "Setting up links..." "info"
	jq -r 'keys[]' "$MODULES_FILE" | while read -r module; do
		links=$(jq ".$module | if has(\"links\") then .links | keys[] else \"\" end" "$MODULES_FILE")
		for link in $links; do
			target_directory=$(jq -r ".$module.target_directory" "$MODULES_FILE")
			target_directory="$(eval echo ${target_directory})"  # This is done to expand env vars like $HOME
			source="${target_directory}/$(jq -r ".$module.links[$link].source" "$MODULES_FILE")"
			target=$(jq -r ".$module.links[$link].target" "$MODULES_FILE")
			target="$(eval echo ${target})"  # This is done to expand env vars like $HOME
			[[ "${target}" == null ]] && continue
			install_link "${source}" "${target}"
		done
	done
}


main || except 1 "Failure in main function!"
exit 0
